package ru.vodomerkin.react.poc.poc.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.vodomerkin.react.poc.poc.data.Link;

/**
 * Created by Endeg on 16.10.2017.
 */
@RepositoryRestResource(collectionResourceRel = "links", path = "links")
public interface LinkRepository extends PagingAndSortingRepository<Link, Integer> {
}
