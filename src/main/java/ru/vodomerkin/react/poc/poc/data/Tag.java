package ru.vodomerkin.react.poc.poc.data;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Endeg on 16.10.2017.
 */
@Entity
@Table(name = "tags")
public class Tag {

    @Id
    @Column(name = "tag_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "l_links_tags",
            joinColumns = {@JoinColumn(name = "tag_id", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "link_id", nullable = false, updatable = false)})
    private List<Link> links;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (id != null ? !id.equals(tag.id) : tag.id != null) return false;
        return name != null ? name.equals(tag.name) : tag.name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", links=[" + getLinks().stream().map(x -> x.getId().toString()).collect(Collectors.joining(", ")) + ']' +
                '}';
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
