package ru.vodomerkin.react.poc.poc.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.vodomerkin.react.poc.poc.data.Tag;

/**
 * Created by Endeg on 16.10.2017.
 */
@RepositoryRestResource(collectionResourceRel = "tags", path = "tags")
public interface TagRepository extends PagingAndSortingRepository<Tag, Integer> {
}
