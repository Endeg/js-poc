package ru.vodomerkin.react.poc.poc.repo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

/**
 * Created by Endeg on 16.10.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class LinkRepositoryTest {

    @Autowired
    LinkRepository linkRepository;

    @Test
    public void checkLinkReadability() {
        linkRepository.findAll().forEach(RepoTestFuncs::testPrint);
    }
}